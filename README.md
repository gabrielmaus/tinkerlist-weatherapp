# tinkerlist

## Project setup
```
npm install
```
### .env file
```
configure your DarkSky Api key and your Google api key
```
### Compiles and hot-reloads for development
```
npm run serve
```

### Compiles and minifies for production
```
npm run build
```

### Lints and fixes files
```
npm run lint
```

### Customize configuration
See [Configuration Reference](https://cli.vuejs.org/config/).
