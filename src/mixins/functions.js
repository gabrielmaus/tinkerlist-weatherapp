export default {
    data: function () {
      return {
        date: null,
        utcString: null,
        day: null,
        time: null,
        previousDates: []
      }
    },
    methods: {
        unixTimeToDate: function(time) {
            var dateObj = new Date(time * 1000)
            var utcString = dateObj.toString()
            var day = utcString.slice(0, 15)
            return day            
        },
        unixTimeToTime: function(time) {
          var dateObj = new Date(time * 1000)
          var utcString = dateObj.toString()
          time = utcString.slice(16, 21)
          return time           
        },
        getIcon: function(icon) {
          return require('../assets/wIcons/'+icon+'.png')
        },
        getPreviousDates: function (days) {
          var previousDates = []
          for (let i = 0 ; i < days; i++) {
            var date = new Date()
            date.setDate(date.getDate() - i)
            previousDates.push((date.getTime() / 1000).toFixed(0)) 
          }
          return previousDates
        },
        displayToaster: function(title, message, type) {
          return this.$bvToast.toast(message, {
            title: title,
            autoHideDelay: 3000,
            variant: type,
            solid: true
          })
        }
    }
  };