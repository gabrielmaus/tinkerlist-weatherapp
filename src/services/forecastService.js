import axios from 'axios'

import forecastApiEndpoint from './endpoints/forecastApiEndpoint'
export default ({

  getForecast: function (latitude, longitude) {
    return axios.get(forecastApiEndpoint.endpoint() + latitude+','+longitude)
  },
  getForecastByTime: function (latitude, longitude, date) {
    return axios.get(forecastApiEndpoint.endpoint() + latitude+','+longitude+','+date)
  },
  getMultipleForecastsByTime: function (latitude, longitude, dates) {
    var requests = []
    for (let i = 0; i < dates.length; i++) {
      var request = this.getForecastByTime(latitude, longitude, dates[i])
      requests.push(request)
    }
    return axios.all(requests)
  }
})