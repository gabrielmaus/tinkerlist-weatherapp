
const apiKey = process.env.VUE_APP_DARKSKY_API_KEY
const endpoint = 'https://cors-anywhere.herokuapp.com/https://api.darksky.net/forecast/'
export default ({
  apiKey() {
    return apiKey
  },
  endpoint() {
    return endpoint + apiKey + '/'
  }
})