
const apiKey = process.env.VUE_APP_GOOGLE_API_KEY
const geocodingEndpoint = 'https://maps.googleapis.com/maps/api/geocode/json'
const geolocationEndpoint = 'https://www.googleapis.com/geolocation/v1/geolocate'

export default ({
  apiKey() {
    return apiKey
  },
  geocodingEndpoint() {
    return geocodingEndpoint
  },
  geolocationEndpoint() {
    return geolocationEndpoint
  }
})