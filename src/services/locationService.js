import axios from 'axios'

import googleApiEndpoint from './endpoints/googleApiEndpoint'
export default ({

  getCurrentLocation () {
    return axios.post(googleApiEndpoint.geolocationEndpoint()+'?key='+googleApiEndpoint.apiKey())
  },
  getLocationNameByCoordinates (latitude, longitude) {
    return axios.get(googleApiEndpoint.geocodingEndpoint()+'?latlng='+latitude+','+longitude+'&result_type=administrative_area_level_2&key='+googleApiEndpoint.apiKey())
  },
  getCoordinatesByLocationName (locationName) {
    return axios.get(googleApiEndpoint.geocodingEndpoint()+'?address='+locationName+'&key='+googleApiEndpoint.apiKey())
  },
})
